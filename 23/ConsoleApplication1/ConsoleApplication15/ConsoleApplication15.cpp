
#include "stdafx.h"
#include <iostream>
#include <array>
#include <vector>
#include <list>
#include <string>
#include <map>
#include <cmath>
#include <exception>

//STD-стандартныя библеотека языка C++
//функции printf? getchar, fopen, read - из данной библеотеки
// является связующим звеном 
//[C++] -> [STD] -> [OC]

//STL - standart template library(template - шаблон)
// это расшир STD, содерж структуры данных 
// и фун по работе с ними 
// именно из STL подкл потоки cout, cin, fstream, ifstream, ofstream, 
// контейнеры array, vecotr, list, map 
// итераторы 
// алгоритмы sort, copy и т.д.


//std::array 
//инкапсулирует массивы неизменной длины, является шаблоном 

//std::vector


//std::list


//std::map

int main()
{
	std::array<double, 10> double_array1; //объект, построенный из шаблона std::array<>, инкапсулирующий массив

	double_array1[5] = 10.5;
	double_array1.fill(0.0);
	int array_lenght = double_array1.size();
	//	double_array1.swap() - функция для обмена содержимым двух массивов 
	std::sort(double_array1.begin(), double_array1.end());
	std::array<double, 10> double_array2 = { 0 };
	std::copy(double_array1.begin(), double_array1.end(), double_array2.begin());



	//для созд двумерного массивов
	std::array<std::array<int, 3>, 3> mat33;

	//итератор, объект ссылающийся на элемент массива, вектора или списка
	//"улучшенный" вариант переменной-счётчика
	//нужен для связи STL
	std::array<double, 10>::iterator array_iterator;

	//std::vector
	//инкапсулирует одномерный массив и предоставляет средства 
	//для измерения его длинны
	//очень близким к std::vector по функционалу является std::string
	//ОСНОВНОЙ НЕДОСТАТОК
	//каждый раз при увеличении длинны vector заприашивает у ОС 
	//новую облать памяти и копирует туда старую (старую освобождает)
	//ОСНОВНОЕ ПРЕИМУЩЕСТВО
	//Доступ к элементам осуществяется напрямую по адресу
	//[адрес_1 + i]
	//ВЫВОД
	//Доступ быстрый, а вставка или увелечение медленные
	std::cout << "vector<> =";
	std::vector<long long> long_vector = { 50, 10, 20 };
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;
	
	long_vector.push_back(25); 
	long_vector.push_back(30);

	std::cout << "vector<> after push =";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;

	//присутсвует весь функционал std::array:
	//размер, сортировка, итераторы, копирование, обмен и т.д.

	std::sort(long_vector.begin(), long_vector.end());
	std::cout << "vector<> after sort() =";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;

	long_vector[2] = 11;

	//вставкка в вектор
	long_vector.insert(long_vector.begin() + 2, 100500/*с помощью шаблона можно вставить несколько значений*/);
	std::cout << "vector<> after insert() =";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;


	//std::list
	//вставка или увелечение списка производится быстрее чем вектор,
	//а доступ - медленне, причём чем дальше от 1-го элемента - тем МЕДЛЕНЕЕ
	//вставка медленее, так как для доступа к i-му эллементу (определении его адреса в ОП) необходимо 
	//пройти по цепочке от i-го элемента
	//в отличие от array и vector не является компактным массивом в памяти,
	//элементы std::list представленны отдельными объектами, связанными друг с другом
	//указателями, которые они хранят
	//[null|данные|указатель] --->  [указатель|данные|указатель]
	//												 /
	//												/
	//											   /
	//											[указатель|данные|указатель] ---> [укзатель|данные|null]

	std::list<char> char_list = { 'b' };
	char_list.push_front('a');//вставка перед первым элементом
	char_list.push_back('c');//вставка после последнего
	for (char ch : char_list)
	{
		std::cout << ch << std::endl;
	}

	//если класс простой и не содержит динамических данных
	class sample_class
	{
		int a;
		double b;
	};

	//список можно составлять из объектов класса
	std::list<sample_class> obj_list1;
	// в противном случае - из указателей на объекты
	std::list<sample_class*> obj_list2;


	//std::map
	//состоит из пар "ключ" - значение
	//например "Tu-154" - 5000, "B777" - 6000, "A320" - 6500

	std::map<std::string, double> plane_and_range =
	{ { "Tu-154" , 5000 },{ "B777" , 6000 },{ "A320" , 6500 } };
	//plane_and_range.insert({ "A380", 6500 });
	std::cout << "The range of Boing 777 is " << plane_and_range["B777"] << std::endl;
	//std::cout << "The range of A380 is " << plane_and_range["A380"] << std::endl;
	plane_and_range.insert({ "A380", 6500 });
	std::cout << "The range of A380 is " << plane_and_range["A380"] << std::endl;


	double d_array[5] = { 1, 2, 3, 4, 5 };
	int i = 60000;
	try // в try помещается код, потенциально способный вызвать исключения:
		// функции, не получавшие доступа к файлам, портам, устройсвам, и т.д.
		//функции, неправильно работающие с памятю
	{
		d_array[i] = 15;
	}
	catch (/*код ошибки*/std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Error in \"d_array[i] = 15\"" << std::endl;
	}



	getchar();
	getchar();
	return 0;
}

//в курс не вошли темы необходимые для штатной комерческой разработки ПО
// - юнит-тестирование ( на каждый модуль или проект создаётся проект, который производит тестирование, сборка и запуск sln
// настраивается таким образом, что в первую очередь собирается базовый проект, затем код теста и запускается)
// - технологии, процессы и шаблоны разработки (SCRUM, Waterfall, Agile, ...)
// - создание графических приложений 
// - многопоточные и асинхронные приложения


