#include "stdafx.h"
#include <iostream>
void fnc1()//функция для переполнения стека
{
	//int b[1000][1000] = { 0 };
}

int main()
{
	// Все переменные которые создаются внутри функций выделяютсся в стековой памяти
	// Стековая память ограниченна, при неправильном использовании стек переполняется (stack overflow)
	// Размерность массивов в стековой памяти фиксированны и задаются константами

	int l1 = 10, l2 = 15;
	// int arr[l1][l2] = { 0 }; в таком обьявлении только константы

	//для использования свободной памяти OC (heap - "куча")
	//обьявляются динамические переменные по синтаксису
	//указатель_на_тип имя_переменной = new тип
	//Тип *имя_переменной = new тип;
	//работа с динамической памятью происходит через указатели


	//Преимущества
	//1)доступно больше памяти нежели в стеке
	//2)размеры массивов и структур данных можно определять и менять во время работы приложения

	//Особенность:
	//если программа предназначенна для длительной работы
	//во избежания переполнения памяти созданные переменые и блоки памяти должны быть
	//освобожденны после использования
	//c помощью оператора delete:
	// delete имя_переменной;

	int * ptr_int = new int; // в нераспределённой памяти создаётся блок
							 //размером с int и указатель на него возращается в переменную ptr_int


							 //вызов утечки памяти

	for (int i = 0; i < 1000; i++)
	{
		//1)каждый заход в  стеке создаётся новый указатель
		//2) выделяется блок long long; в куче(нераспределённой памяти приписнной этой программе
		//3)Стековая переменная b изчезает;
		//4)а блок по-прежнему остаётся там же
		//5) при следующем заходе выделяется новый блок
		long long * b = new long long;
		//совершить нужные действия с переменной 
		delete b;//удалить блок памяти пока не потерян указатель на неё
	}

	//пара операторов new/delete - это одно из нововведений С++ относительно С

	//в динамической памяти могут выделяться только одномерные массивы
	//тип * имя_указателя = new тип[длина]
	//удаляются одномерные массивы с помощью записи
	//  delete [] имя_указателя;


	//для выделения двумерного массива сначала создаётся одномерный массив, содержащий указатели на другие массивы (строки)
	//затем выделятеся блок памяти под каждую строку и указатель на неё возращается в созданный массив указателей

	int** dynamic2darray = new int *[l1];
	for (/*int f = 0; f < 5; f++*/;;)
	{
		for (int i = 0; i < l1; i++)
		{
			dynamic2darray[i] = new int[l2];//dynamic2darray[i]  тип int* 
		}
	}
	for (int i = 0; i < l1; i++)
	{
		delete[] dynamic2darray[i];//dynamic2darray[i]  тип int* 
	}

	delete[] dynamic2darray;

	//многомерные массивы фиксированной длинны в стековой памяти чаще всего размещаются сплошным блоком
	//динамические - нет (где OC выделило место под очередную строку - там она и будет находится и между строками будет неизвестно что

	int fixed_array[3][3] = { { 10, 20, 30 },
	{ 40, 50, 60 },
	{ 70, 80 ,90 } };
	for (int * pointer = &fixed_array[0][0] - 10; pointer < &fixed_array[0][0] + 10; pointer++)
	{
		//std::cout <</*указатель*/<<"\t"<</*значение*/<<std::endl;
		std::cout << "0x" << pointer << "\t" << *pointer << std::endl;
	}

	getchar();
	return 0;
}
