#include "stdafx.h"
#include "Array.h"
#include <iostream>



Matrice::Matrice()
{
	** Mirror_Arr = NULL;
	** Arr = NULL;
	nRow = NULL;
	nCol = NULL;
}

Matrice::Matrice(int row, int col)
{
	nRow = row;
	nCol = col;
	Mirror_Arr = new int *[nRow];
	Arr = new int *[nRow];

	for (int i = 0; i < nRow; i++)
	{
		Mirror_Arr[i] = new int[nCol];
		Arr[i] = new int[nCol];
	}
}

void Matrice::fill_arr()
{
	for (int i = 0; i < nRow; i++)
	{
		for (int j = 0; j < nCol; j++)
		{
			Arr[i][j] = rand() % 10;
			Mirror_Arr[i][j] = Arr[i][j];
		}
	}
}

void Matrice::Arr_out()
{
	for (int i = 0; i < nRow; i++)
	{
		for (int j = 0; j < nCol; j++)
		{
			std::cout << Arr[i][j] << '\t';
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void Matrice::Arr_out_for_skal()
{
	for (int i = 0; i < nRow; i++)
	{
		std::cout << Arr[i][0] << std::endl;
	}

	std::cout << std::endl;
}

void Matrice::add_matr_or_vec(int **& matr_a, int **& matr_b)
{

	//   
	for (int i = 0; i < nRow; i++)
	{
		for (int j = 0; j < nCol; j++)
		{
			Arr[i][j] = 0;
			Arr[i][j] += matr_a[i][j] + matr_b[i][j];
		}
	}
}

void Matrice::minus(int **& matr_a, int **& matr_b)
{
	for (int i = 0; i < nRow; i++)
	{
		for (int j = 0; j < nCol; j++)
		{
			Arr[i][j] = 0;
			Arr[i][j] += matr_a[i][j] - matr_b[i][j];
		}
	}
}



void Matrice::mult_arr(int **& matr_a, int **& matr_b, int row_a, int col_a, int col_b)
{
	for (int i = 0; i < row_a; i++)
	{
		for (int j = 0; j < col_b; j++)
		{
			Arr[i][j] = 0;
			for (int k = 0; k < col_a; k++)
				Arr[i][j] += matr_a[i][k] * matr_b[k][j];

		}
	}
}

void Matrice::mult_vec_and_matr_OR_skal_vec(int **& vec_a, int **& matr_b, int row, int col)
{
	//    
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			Arr[i][j] = 0;
			Arr[i][0] += matr_b[i][j] * vec_a[0][j];
		}
	}
}

void Matrice::delete_array()
{
	for (int i = 0; i < nRow; i++)
	{
		delete[] Mirror_Arr[i];
		delete[] Arr[i];

	}
	delete[] Mirror_Arr;
	delete[] Arr;
}

Matrice::~Matrice()
{
}

void Vector::vec_multiplication_vec(int ** &v_a, int ** &v_b)
{
	//  


	std::cout << std::endl;
	std::cout << std::endl;
	int vec_rez[1][3] = { 0, 0 ,0 };

	vec_rez[0][0] = v_a[0][1] * v_b[0][2] - v_a[0][2] * v_b[0][1];//1-  
	vec_rez[0][1] = v_a[0][2] * v_b[0][0] - v_a[0][0] * v_b[0][2];//2-  
	vec_rez[0][2] = v_a[0][0] * v_b[0][1] - v_a[0][1] * v_b[0][0];//3-  


	std::cout << "Vector Rez = " << std::endl;
	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << vec_rez[i][j] << '\t';//    
		}

	}

	std::cout << std::endl;
	std::cout << std::endl;
}
